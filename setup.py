from setuptools import setup

setup(name='misago_ipsdatamover',
      version='0.1',
      description='An application to migrate forum from IPSv4 to Misago',
      url='http://github.com/NotKit/misago_ipsdatamover',
      author='TheKit',
      author_email='nekit@sonicscanf.org',
      license='GPLv2',
      packages=['misago_ipsdatamover'],
      zip_safe=False)

from __future__ import unicode_literals

from django.db import transaction
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.utils.text import slugify

from misago.categories.models import Category
from misago.threads.checksums import update_post_checksum
from misago.threads.models import (
    Thread, ThreadParticipant, Post, PostEdit, PostLike)

from . import (fetch_assoc, fetch_assoc_many, markup, movedids,
               localise_datetime, timestamp_to_datetime, fix_ip_address)


UserModel = get_user_model()

@transaction.atomic
def move_threads(stdout, style):

    Thread.objects.all().delete()
    threads = []
    old_ids = []

    for thread in fetch_assoc('SELECT * FROM ipb_forums_topics ORDER BY tid'):
        if not thread['topic_firstpost']:
            stdout.write(style.ERROR(
                "Corrupted thread: %s" % thread['title']))
            continue

        category_id = movedids.get('category', thread['forum_id'])

        new_thread = Thread(
            category_id=category_id,
            title=thread['title'],
            slug=thread['title_seo'],
            started_on=timezone.now(),
            last_post_on=timezone.now(),
            starter_name='none',
            starter_slug='none',
            weight=thread['pinned'],
            is_hidden=False,
            is_closed=(thread['state'] == 'closed'),
        )
        threads.append(new_thread)
        old_ids.append(thread['tid'])

    Thread.objects.bulk_create(threads)
    movedids.set_bulk('thread', old_ids, [thread.pk for thread in threads])


@transaction.atomic
def move_posts():
    Post.objects.all().delete()

    thread_categories = dict(Thread.objects.all().values_list('pk', 'category_id'))

    for rows in fetch_assoc_many('SELECT * FROM ipb_forums_posts ORDER BY pid', size=10000):
        posts = []
        old_ids = []

        for post in rows:
            if post['pdelete_time'] != 0:
                print(post)

            thread_pk = movedids.get('thread', post['topic_id'])

            poster_id = None
            last_editor_id = None

            if post['author_id']:
                poster_id = movedids.get('user', post['author_id'])

            new_post = Post(
                category_id=thread_categories[thread_pk],
                thread_id=thread_pk,
                poster_id=poster_id,
                poster_name=post['author_name'],
                poster_ip=fix_ip_address(post['ip_address']),
                original=post['post'],
                parsed=post['post'],
                posted_on=timestamp_to_datetime(post['post_date']),
                updated_on=timestamp_to_datetime(post['edit_time'] or post['post_date']),
                hidden_on=timestamp_to_datetime(post['pdelete_time'] or post['post_date']),
                edits=1 if post['edit_time'] else 0,
                last_editor=None,
                last_editor_name=post['edit_name'],
                last_editor_slug='',
                is_hidden=bool(post['pdelete_time']),
                is_protected=False,
                likes=0
            )
            update_post_checksum(new_post)
            posts.append(new_post)
            old_ids.append(post['pid'])

        Post.objects.bulk_create(posts)
        movedids.set_bulk('post', old_ids, [post.pk for post in posts])


@transaction.atomic
def move_private_threads(stdout, style):
    category = Category.objects.private_threads()

    Thread.objects.filter(category=category).delete()
    threads = []
    old_ids = []

    for topic in fetch_assoc('SELECT * FROM ipb_core_message_topics ORDER BY mt_id'):
        new_thread = Thread(
            category_id=category.id,
            title=topic['mt_title'],
            slug=(slugify(topic['mt_title'], allow_unicode=True) or str(topic['mt_id'])),
            started_on=timezone.now(),
            last_post_on=timezone.now(),
            starter_name='none',
            starter_slug='none',
            weight=0,
            is_hidden=topic['mt_is_deleted'],
            is_closed=False,
        )
        threads.append(new_thread)
        old_ids.append(topic['mt_id'])

    Thread.objects.bulk_create(threads)
    movedids.set_bulk('private_thread', old_ids, [thread.pk for thread in threads])


@transaction.atomic
def move_private_posts(stdout, style):
    category = Category.objects.private_threads()
    Post.objects.filter(category=category).delete()

    for rows in fetch_assoc_many('SELECT * FROM ipb_core_message_posts ORDER BY msg_id', size=10000):
        posts = []
        old_ids = []

        for post in rows:
            thread_pk = movedids.get('private_thread', post['msg_topic_id'])
            poster_id = None

            if post['msg_author_id']:
                poster_id = movedids.get('user', post['msg_author_id'])

            new_post = Post(
                category=category,
                thread_id=thread_pk,
                poster_id=poster_id,
                poster_name='',
                poster_ip=fix_ip_address(post['msg_ip_address']),
                original=(post['msg_post'] or ''),
                parsed=(post['msg_post'] or ''),
                posted_on=timestamp_to_datetime(post['msg_date']),
                updated_on=timestamp_to_datetime(post['msg_date']),
                hidden_on=timestamp_to_datetime(post['msg_date']),
                edits=0,
                last_editor=None,
                last_editor_name='',
                last_editor_slug='',
                is_hidden=False,
                is_protected=False,
                likes=0
            )
            update_post_checksum(new_post)
            posts.append(new_post)
            old_ids.append(post['msg_id'])

        Post.objects.bulk_create(posts)
        movedids.set_bulk('private_post', old_ids, [post.pk for post in posts])


@transaction.atomic
def move_likes():
    PostLike.objects.all().delete()

    query = "SELECT * FROM ipb_core_reputation_index WHERE app = 'forums' ORDER by id"

    posts = {}
    post_likes = []

    for reputation in fetch_assoc(query):
        assert reputation['type'] == 'pid'
        post_pk = movedids.get('post', reputation['type_id'])

        if not post_pk:
            print("Like for unknown post id: {}".format(reputation['type_id']))
            continue

        if post_pk not in posts:
            post = Post.objects.only('pk', 'category_id', 'thread_id', 'likes', 'last_likes').get(pk=post_pk)
            post.likes = 0
            post.last_likes = []
        else:
            post = posts[post_pk]

        if reputation['member_id']:
            liker_pk = movedids.get('user', reputation['member_id'])
            if not liker_pk:
                print("Like from unknown user id: {}".format(reputation['member_id']))
                continue

            liker = UserModel.objects.get(pk=liker_pk)
        else:
            liker = None

        post_like = PostLike(
            category_id=post.category_id,
            thread_id=post.thread_id,
            post=post,
            liker=liker,
            liker_name=liker.username if liker else "",
            liker_slug=liker.slug if liker else "",
            liker_ip="127.0.0.1",
            liked_on=timestamp_to_datetime(reputation['rep_date']),
        )
        post_likes.append(post_like)

        post.likes += 1
        post.last_likes.append({
            'id': post_like.liker_id,
            'username': post_like.liker_name
        })

    PostLike.objects.bulk_create(post_likes)

    for post in posts.values():
        post.last_likes = post.last_likes[:4]
        post.save(update_fields=['likes', 'last_likes'])


@transaction.atomic
def move_participants():
    ThreadParticipant.objects.all().delete()

    participants = []

    for participant in fetch_assoc('SELECT * FROM ipb_core_message_topic_user_map'):
        thread_id = movedids.get('private_thread', participant['map_topic_id'])
        user_id = movedids.get('user', participant['map_user_id'])

        if participant['map_is_system']:
            # It's notification sent on behalf of user by system, skip him as partiticant
            continue

        if not thread_id or not user_id:
            print("Unknown thread: {} or participant: {}, skipping".format(thread_id, user_id))
            continue

        thread = Thread.objects.get(pk=thread_id)
        starter_id = thread.post_set.order_by('id').first().poster_id

        new_partiticipant = ThreadParticipant(
            thread=thread,
            user_id=user_id,
            is_owner=(user_id == starter_id)
        )
        participants.append(new_partiticipant)

    ThreadParticipant.objects.bulk_create(participants)


def clean_private_threads(stdout, style):
    category = Category.objects.private_threads()

    # prune threads without participants
    participated_threads = ThreadParticipant.objects.values_list(
        'thread_id', flat=True).distinct()
    for thread in category.thread_set.exclude(pk__in=participated_threads):
        thread.delete()

    # close threads with single participant, delete empty ones
    for thread in category.thread_set.iterator():
        participants_count = thread.participants.count()
        if participants_count == 1:
            thread.is_closed = True
            thread.save()
        elif participants_count == 0:
            thread.delete()
            stdout.write(style.ERROR(
                "Delete empty private thread: %s" % thread.title))


def get_special_categories_dict():
    special_categories = {}

    query = '''
        SELECT
            id, special
        FROM
            misago_forum
        WHERE
            special IS NOT NULL
    '''

    for special in fetch_assoc(query):
        special_categories[special['id']] = special['special']
    return special_categories

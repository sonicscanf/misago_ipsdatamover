import ipaddress
from datetime import datetime, timezone
from django.core.validators import ip_address_validators
from django.core.exceptions import ValidationError
from django.utils.timezone import make_aware, utc


from .conf import OLD_FORUM
from .db import fetch_assoc, fetch_assoc_many


def localise_datetime(datetime):
    if datetime:
        return make_aware(datetime, utc)
    return None

def timestamp_to_datetime(timestamp):
    if timestamp:
        return datetime.fromtimestamp(timestamp, timezone.utc)
    return None

def fix_ip_address(ip_address):
    # Not using Django validation here, since it will pass IP like
    # '2601:8:a200:60f', while PostgreSQL will not

    try:
        ipaddress.ip_address(ip_address)
    except ValueError:
        return '127.0.0.1'
    else:
        return ip_address

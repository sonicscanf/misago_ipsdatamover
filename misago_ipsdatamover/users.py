from __future__ import unicode_literals

from django.db import transaction
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.crypto import get_random_string

from misago.conf.models import Setting
from misago.users.models import UsernameChange
from misago.users.signatures import make_signature_checksum

from . import fetch_assoc, movedids
from .utils import localise_datetime, timestamp_to_datetime, \
    fix_ip_address


UserModel = get_user_model()


PRIVATE_THREAD_INVITES = {
    0: 0,
    1: 0,
    2: 1,
    3: 2
}


@transaction.atomic
def move_users(stdout, style):
    existing_users = get_existing_users()
    
    username_length_max = Setting.objects.get(setting="username_length_max")
    username_length_max.value = 64
    username_length_max.save()

    stdout.write("Setting username_length_max to %d" % username_length_max.value)
    
    for user in fetch_assoc('SELECT * FROM ipb_core_members ORDER BY member_id'):
        if user['email'].lower() in existing_users:
            user_new_pk = existing_users[user['email'].lower()]
            new_user = UserModel.objects.get(pk=user_new_pk)
        else:
            try:
                new_user = UserModel.objects.create_user(
                    user['name'], user['email'], 'Pass.123')
            except ValidationError as e:
                message = e.messages[0]
                if 'Username' in message:
                    new_name = ''.join([user['name'][:20], get_random_string(4)])
                    new_user = UserModel.objects.create_user(
                        new_name, user['email'], 'Pass.123')
    
                    formats = (user['name'], new_name)
                    stdout.write(style.ERROR(
                        '"%s" has been registered as "%s"' % formats))

                elif message == "This e-mail address is not available.":
                    new_email = ''.join([user['email'], get_random_string(4)])
                    new_user = UserModel.objects.create_user(
                        user['name'], new_email, 'Pass.123')
    
                    formats = (user['name'], new_email)
                    stdout.write(style.ERROR(
                        '"%s" email has been set to "%s"' % formats))

            # Don't care about IPS passwords for now
            # new_user.password = user['password']

        new_user.joined_on =  timestamp_to_datetime(user['joined'])
        new_user.joined_from_ip = fix_ip_address(user['ip_address'])

        new_user.last_login = timestamp_to_datetime(user['last_visit'])
        new_user.last_ip = fix_ip_address(user['ip_address'])

        new_user.title = user['member_title'] or None

        # new_user.requires_activation = user['activation']
        # if new_user.requires_activation > 2:
        #    new_user.requires_activation = 1

        # if user['signature'] and user['signature_preparsed']:
        #     new_user.signature = user['signature']
        #     new_user.signature_parsed = user['signature_preparsed']
        #     new_user.signature_checksum = make_signature_checksum(
        #         user['signature_preparsed'], new_user)

        # new_user.is_signature_locked = user['signature_ban']
        # new_user.signature_lock_user_message = user['signature_ban_reason_user'] or None
        # new_user.signature_lock_staff_message = user['signature_ban_reason_admin'] or None

        # new_user.limits_private_thread_invites_to = PRIVATE_THREAD_INVITES[user['allow_pds']]

        # new_user.subscribe_to_started_threads = int(user['subscribe_start'])
        # new_user.subscribe_to_replied_threads = int(user['subscribe_reply'])

        new_user.save()
        movedids.set('user', user['member_id'], new_user.pk)


def get_existing_users():
    existing_users = {}
    
    UserModel.objects.all().delete()

    queryset = UserModel.objects.values_list('id', 'email')
    for pk, email in queryset.iterator():
        existing_users[email.lower()] = pk
    return existing_users


def move_followers():
    for follow in fetch_assoc('SELECT * FROM misago_user_follows ORDER BY id'):
        from_user_id = movedids.get('user', follow['from_user_id'])
        to_user_id = movedids.get('user', follow['to_user_id'])

        from_user = UserModel.objects.get(pk=from_user_id)
        to_user = UserModel.objects.get(pk=to_user_id)

        from_user.follows.add(to_user)


def move_blocks():
    for follow in fetch_assoc('SELECT * FROM misago_user_ignores ORDER BY id'):
        from_user_id = movedids.get('user', follow['from_user_id'])
        to_user_id = movedids.get('user', follow['to_user_id'])

        from_user = UserModel.objects.get(pk=from_user_id)
        to_user = UserModel.objects.get(pk=to_user_id)

        from_user.blocks.add(to_user)


def move_namehistory():
    query = 'SELECT DISTINCT user_id FROM misago_usernamechange ORDER BY user_id'
    for user in fetch_assoc(query):
        new_id = movedids.get('user', user['user_id'])
        new_user = UserModel.objects.get(pk=new_id)
        move_users_namehistory(new_user, user['user_id'])


def move_users_namehistory(user, old_id):
    username_history = []
    query = 'SELECT * FROM misago_usernamechange WHERE user_id = %s ORDER BY id'
    for namechange in fetch_assoc(query, [old_id]):
        if username_history:
            username_history[-1].new_username = namechange['old_username']

        username_history.append(UsernameChange(
            user=user,
            changed_by=user,
            changed_by_username=user.username,
            changed_on=localise_datetime(namechange['date']),
            new_username=user.username,
            old_username=namechange['old_username']
        ))

    UsernameChange.objects.bulk_create(username_history)

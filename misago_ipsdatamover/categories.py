from __future__ import unicode_literals

from django.db import transaction
from django.utils.html import strip_tags
from django.utils.text import slugify
from django.utils.translation import ugettext as _

from misago.categories.models import Category, CategoryRole, RoleCategoryACL
from misago.acl.models import Role

from . import fetch_assoc, movedids
from .db import get_ipb_core_sys_lang_word


@transaction.atomic
def move_categories(stdout, style):
    query = 'SELECT * FROM ipb_forums_forums ORDER BY parent_id, id'
    
    Category.objects.all_categories().delete()

    root = Category.objects.root_category()
    for forum in fetch_assoc(query):
        #if forum['type'] == 'redirect':
        #    stdout.write(style.ERROR('Skipping redirect: %s' % forum['name']))
        #    continue

        name = get_ipb_core_sys_lang_word('forums', 'forums_forum_{}'.format(forum['id']))
        slug = forum['name_seo'] if forum['name_seo'] else slugify(name, allow_unicode=True)

        if forum['parent_id'] == -1:
            parent = root
        else:
            new_parent_id = movedids.get('category', forum['parent_id'])
            parent = Category.objects.get(pk=new_parent_id)

        category = Category.objects.insert_node(Category(
            name=name,
            slug=slug,
            description=strip_tags(get_ipb_core_sys_lang_word('forums',
                'forums_forum_{}_desc'.format(forum['id']))),
            is_closed=False,
        ), parent, save=True)

        movedids.set('category', forum['id'], category.pk)

        # Set default "sane" permissions
        RoleCategoryACL.objects.create(
            role=Role.objects.get(name=_('Moderator')),
            category=category,
            category_role=CategoryRole.objects.get(name=_('Moderator')),
        )

        RoleCategoryACL.objects.create(
            role=Role.objects.get(special_role='authenticated'),
            category=category,
            category_role=CategoryRole.objects.get(name=_('Start and reply threads, make polls')),
        )

        RoleCategoryACL.objects.create(
            role=Role.objects.get(special_role='anonymous'),
            category=category,
            category_role=CategoryRole.objects.get(name=_('Read only')),
        )


def get_root_tree():
    query = 'SELECT tree_id FROM misago_forum WHERE special = %s'
    for root in fetch_assoc(query, ['root']):
        return root['tree_id']


def move_labels():
    labels = []
    for label in fetch_assoc('SELECT * FROM misago_threadprefix ORDER BY slug'):
        labels.append(label)

    for label in labels:
        query = 'SELECT * FROM misago_threadprefix_forums WHERE threadprefix_id= %s'
        for parent_row in fetch_assoc(query, [label['id']]):
            parent_id = movedids.get('category', parent_row['forum_id'])
            parent = Category.objects.get(pk=parent_id)

            category = Category.objects.insert_node(Category(
                name=label['name'],
                slug=label['slug'],
            ), parent, save=True)

            label_id = '%s-%s' % (label['id'], parent_row['forum_id'])
            movedids.set('label', label_id, category.pk)

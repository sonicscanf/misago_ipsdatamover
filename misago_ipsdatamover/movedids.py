from .models import MovedId

cache = {}

def get_old(model, id):
    try:
        return MovedId.objects.get(model=model, old_id=id).new_id
    except MovedId.DoesNotExist:
        return None

def get(model, id):
    try:
        model_dict = cache[model]
    except KeyError:
        values_list = MovedId.objects.filter(model=model).values_list(
            'old_id', 'new_id')
        model_dict = cache[model] = {int(old_id): int(new_id)
                                     for (old_id, new_id) in values_list}
    try:
        return cache[model][id]
    except KeyError:
        print('KeyError:', model, id)
        return None

def set(model, old_id, new_id):
    MovedId.objects.filter(model=model, old_id=old_id).delete()
    
    MovedId.objects.create(
        model=model,
        old_id=old_id,
        new_id=new_id,
    )

    try:
        cache[model][old_id] = new_id
    except KeyError:
        pass

def set_bulk(model, old_ids, new_ids):
    MovedId.objects.filter(model=model, old_id__in=old_ids)

    movedids = [MovedId(model=model, old_id=old_id, new_id=new_id)
               for old_id, new_id in zip(old_ids, new_ids)]

    MovedId.objects.bulk_create(movedids)

from django.db import connections


def fetch_assoc(query, *args):
    """
    Return all rows from a cursor as a dict
    """
    with connections['ips'].cursor() as cursor:
        cursor.execute(query, *args)

        columns = [col[0] for col in cursor.description]
        row = cursor.fetchone()
        while row:
            yield dict(zip(columns, row))
            row = cursor.fetchone()

def fetch_assoc_many(query, *args, size=10000):
    """
    Return all rows from a cursor as a list of dicts
    """
    with connections['ips'].cursor() as cursor:
        cursor.execute(query, *args)

        columns = [col[0] for col in cursor.description]
        row = cursor.fetchone()
        rows = []

        while row:
            rows.append(dict(zip(columns, row)))
            if len(rows) > size:
                yield rows
                rows = []
            row = cursor.fetchone()

        if rows:
            yield rows

# FIXME: Move to settings or get from IPS database
preferred_lang_id = 3

def get_ipb_core_sys_lang_word(word_app, word_key, lang_id=None):
    query = 'SELECT * FROM ipb_core_sys_lang_words WHERE word_app = %s AND word_key = %s'
    args = [word_app, word_key]

    if lang_id:
        query += ' AND lang_id = %s'
        args.append(lang_id)

    rows = fetch_assoc(query, args)
    for row in rows:
        if row['lang_id'] == preferred_lang_id:
            word = row['word_custom'] if row['word_custom'] else row['word_default']
            return word
    else:
        if rows:
            return rows[0]['word_default']

        return word_key.upper()
